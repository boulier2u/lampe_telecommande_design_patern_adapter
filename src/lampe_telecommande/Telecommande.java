package lampe_telecommande;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Telecommande {

	private List<Appareil> l;

	public Telecommande() {
		this.l = new ArrayList<Appareil>();
	}

	public void ajouterLampe(Appareil l) {
		this.l.add(l);
	}

	public void ajouterHifi(Appareil h) {
		this.l.add(h);
	}

	public void activerLampe(int i) {
		this.l.get(i).allumer();
	}

	public void activerHifi(int k) {
		this.l.get(k).allumer();
	}

	public void desactiverLampe(int j) {
		this.l.get(j).eteindre();
	}

	public void desactiverHifi(int l) {
		this.l.get(l).eteindre();
	}

	public void activerToutLampe() {
		int i = 0;
		Iterator<Appareil> it = this.l.iterator();
		while (it.hasNext()) {
			this.activerLampe(i);
			i++;

		}
	}

	public void activerTout() {
		int i = 0;
		Iterator<Appareil> it = this.l.iterator();

		while (it.hasNext()) {
			this.activerLampe(i);
			this.activerHifi(i);

			i++;

		}

	}

	public void activerToutHifi() {
		int i = 0;
		Iterator<Appareil> it = this.l.iterator();
		while (it.hasNext()) {
			this.activerHifi(i);
			i++;

		}
	}

	public String toString() {
		String s = "";
		Iterator<Appareil> it = this.l.iterator();
		while (it.hasNext()) {
			s += it.next() + "\n";

		}
		return s;
	}

	public List<Appareil> getLampe() {
		return l;
	}
}
