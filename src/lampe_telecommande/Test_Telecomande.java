package lampe_telecommande;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

public class Test_Telecomande {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		fail("Not yet implemented");
	}

	public void test_AjouteLampeVide() {
		Telecommande t = new Telecommande();
		Lampe l1 = new Lampe("lampe1");
		t.ajouterLampe(l1);
		assertEquals("pas de lampe ajouté", t.getLampe().get(0), l1);
	}

	public void test_AjouteLampeAvecElem() {
		Telecommande t = new Telecommande();
		Lampe l1 = new Lampe("lampe1");
		Lampe l2 = new Lampe("lampe2");
		t.ajouterLampe(l1);
		t.ajouterLampe(l2);
		assertEquals("pas de lampe ajouté", t.getLampe().get(1), l2);
	}
	

	public void test_SuppLampe() {
		Telecommande t = new Telecommande();
		Lampe l1 = new Lampe("lampe1");
		t.ajouterLampe(l1);
		t.desactiverLampe(0);
		assertEquals("pas de lampe ajouté", l1.isAllume(), false);
	}

	public void test_ActiverLampe0() {
		Telecommande t = new Telecommande();
		Lampe l1 = new Lampe("lampe1");
		t.ajouterLampe(l1);
		t.activerLampe(0);
		assertEquals("pas de lampe ajouté", l1.isAllume(), true);
	}

	public void test_ActicLampe2() {
		Telecommande t = new Telecommande();
		Lampe l1 = new Lampe("lampe1");
		Lampe l2 = new Lampe("lampe2");
		t.ajouterLampe(l1);
		t.ajouterLampe(l2);
		t.activerLampe(0);
		t.activerLampe(1);
		;
		assertEquals("pas de lampe ajouté", l2.isAllume(), true);
	}

}
