package lampe_telecommande;

/**
 * La classe lampe mod�lise le comportemment d'une lampe
 * 
 * @author chappetd1
 *
 */
public class Lampe implements Appareil {

	/** Nom de la lampe **/
	private String nom;
	/** Etat de la lampe **/
	private boolean allume;

	/**
	 * Construit une lampe avec le nom donn�
	 * 
	 * @param nom
	 */
	public Lampe(String nom) {
		this.nom = nom;
		this.allume = false;
	}

	/**
	 * Allume la lampe
	 */
	public void allumer() {
		this.allume = true;
	}

	/**
	 * Eteit la lampe
	 */
	public void eteindre() {
		this.allume = false;
	}

	/**
	 * retourne le descriptif de la lampe sous la forme nom + "On"/"Off"
	 */
	public String toString() {
		String r = "";
		if (this.allume) {
			r = "On";
		} else {
			r = "Off";
		}
		return (nom + ": " + r);
	}

	/**
	 * 
	 * @return l'�tat de la lampe
	 */
	public boolean isAllume() {
		return allume;
	}

}