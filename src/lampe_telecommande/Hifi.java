package lampe_telecommande;

public class Hifi implements Appareil {
	int son = 0;

	public Hifi(int son) {
		if (son < 0) {
			this.son = 0;
		} else {
			this.son = son;
		}

	}

	public void allumer() {
		this.son += 10;
		// son maximum
		if (this.son > 100)
			this.son = 100;

	}

	public void eteindre() {
		this.son = 0;
	}

	public String toString() {
		String r = "";
		r += "Hifi :" + son;
		return (r);
	}

}
