package lampe_telecommande;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class Test_Lampe {
	private Lampe lampe;

	@Before
	public void setUp() throws Exception {
		this.lampe = new Lampe("1");
	}

	/**
	 * Methode allumer
	 */
	@Test
	public void testAllumer() {
		this.lampe.allumer();
		assertTrue("La lampe devrait etre allumee", this.lampe.isAllume());
	}

	/**
	 * Methode eteindre
	 */
	@Test
	public void testEteindre() {
		this.lampe.eteindre();
		assertFalse("La lampe devrait etre eteinte", this.lampe.isAllume());
	}

	/**
	 * Methode: to string Scenario: off
	 */
	@Test
	public void testToString_off() {
		String obtenu = this.lampe.toString();
		String attendu = "1: Off";
		assertEquals("L'affichage devrait etre le off.", attendu, obtenu);
	}

	/**
	 * Methode: to string Scenario: on
	 */
	@Test
	public void testToString_on() {
		this.lampe.allumer();
		String obtenu = this.lampe.toString();
		String attendu = "1: On";
		assertEquals("L'affichage devrait etre le on.", attendu, obtenu);
	}

	public Lampe getLampe() {
		return lampe;
	}
}